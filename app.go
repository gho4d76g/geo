package main

import (
	"fmt"
	"io"
	"os"

	colorable "github.com/mattn/go-colorable"
)

// App
type App struct {
	name    string
	version string
	logger  *AppLogger
}

type AppLogger struct {
	debug  bool
	stdout io.Writer
	stderr io.Writer
}

// NewApp return *App
func NewApp() *App {
	var debug bool
	envDebug := os.Getenv("DEBUG")
	if envDebug == "true" {
		debug = true
	}
	// init logger
	return &App{
		name:    "cli-subcommand_sample",
		version: "0.0.1",
		logger: &AppLogger{
			debug:  debug,
			stdout: colorable.NewColorableStdout(),
			stderr: colorable.NewColorableStderr(),
		},
	}
}

// Fatal
func (applogger *AppLogger) Fatal(err error) {
	fmt.Println(err)
}

// Fatalf print error log into applogger.stderr
func (applogger *AppLogger) Fatalf(format string, a ...interface{}) {
	fmt.Fprintf(applogger.stderr, format, a...)
}

// Fprintf print error log into applogger.stdout
func (applogger *AppLogger) Fprintf(format string, a ...interface{}) {
	fmt.Fprintf(applogger.stdout, format, a...)
}

// Debug
func (applogger *AppLogger) Debug(format string, a ...interface{}) {
	if applogger.debug {
		fmt.Fprintf(applogger.stdout, "DEBUG: "+format, a...)
	}
}
