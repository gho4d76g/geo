# geo

A command-line tool for searching the IP address information from GeoIP2/GeoLite2 database.

## Usage

```
Usage: cli-subcommand_sample [--version] [--help] <command> [<args>]

Available commands are:
    pearse    pearse Geo-data of IP address passed from standard input
```

### Subcommand: pearse

```
Usage of getGeo:
  -d string
        PATH to GeoIP2/GeoLite2 database file (default "GeoLite2-City.mmdb")
  -database string
        PATH to GeoIP2/GeoLite2 database file (default "GeoLite2-City.mmdb")
  -o string
        output format (default "json")
  -output string
        output format (default "json")
```

examples:

```
> type test\iplist.dat | .\geo.exe pearse -o json -d test\GeoLite2-City.mmdb
```

output(json)

```json
{
  "geo_ips":[
    {
      "ip_address": "185.172.110.38",
      "country_code": "NL",
      "country": "Netherlands",
      "city": "",
      "time_zone": "Europe/Amsterdam"
    },
    {
      "ip_address": "119.6.13.178",
      "country_code": "CN",
      "country": "China",
      "city": "Chengdu",
      "time_zone": "Asia/Shanghai"
    },
    {
      "ip_address": "147.231.58.21",
      "country_code": "CZ",
      "country": "Czechia",
      "city": "",
      "time_zone": "Europe/Prague"
    },
    {
      "ip_address": "62.141.37.70",
      "country_code": "DE",
      "country": "Germany",
      "city": "",
      "time_zone": ""
    },
    ...中略...
    {
      "ip_address": "211.159.159.137",
      "country_code": "CN",
      "country": "China",
      "city": "Beijing",
      "time_zone": "Asia/Shanghai"
    },
    {
      "ip_address": "185.172.110.230",
      "country_code": "NL",
      "country": "Netherlands",
      "city": "",
      "time_zone": "Europe/Amsterdam"
    },
  ]
}
```
