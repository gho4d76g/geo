package main

import (
	"encoding/json"
	"fmt"
	"io"
	"net"
	"path/filepath"
	"text/tabwriter"

	"github.com/olekukonko/tablewriter"
	"github.com/oschwald/geoip2-golang"
)

const (
	DefaultDictionaryLanguage = "en"
)

// GeoDataSet represent Geo
type GeoDataSet struct {
	GeoIPs []*GeoData `json:"geo_ips"`
}

func (d GeoDataSet) Fprint(w io.Writer) {
	table := tablewriter.NewWriter(w)
	table.SetHeader([]string{"IPAddr", "CountryCode", "City", "TimeZone"})
	for _, v := range d.GeoIPs {
		table.Append([]string{v.IPAddr, v.CountryCode, v.City, v.TimeZone})
	}

	table.Render()
}

func (d GeoDataSet) FprintTsv(w io.Writer) {
	tw := new(tabwriter.Writer)
	// Format in tab-separated columns with a tab stop of 8.
	tw.Init(w, 0, 8, 0, '\t', 0)
	fmt.Fprintln(w, "IPAddress\tCountryCode\tCountryName\tCity\tTimeZone\t")

	for _, v := range d.GeoIPs {
		fmt.Fprintf(tw, "%s\t%s\t%s\t%s\t%s\n", v.IPAddr, v.CountryCode, v.CountryName, v.City, v.TimeZone)
	}
	//fmt.Fprintln(w)
	tw.Flush()
}

func (d GeoDataSet) FprintJson(w io.Writer) {
	b, _ := json.Marshal(d)
	fmt.Fprintln(w, string(b))
}

// GeoData represent Geo
type GeoData struct {
	IPAddr string `json:"ip_address"`
	// DomainName  string `json:"domain_name"`
	CountryCode string `json:"country_code"`
	CountryName string `json:"country"`
	City        string `json:"city, omitempty"`
	TimeZone    string `json:"time_zone"`
}

// Dictionary represent GeoIP Database file Object
type Dictionary struct {
	lang string
	db   *geoip2.Reader
}

func NewDictionary(sPath, lang string) (*Dictionary, error) {
	file_path := filepath.Dir(sPath)
	file_name := filepath.Base(sPath)
	db, err := geoip2.Open(filepath.Join(file_path, file_name))
	if err != nil {
		return nil, err
	}
	// defer db.Close()

	if lang == "" {
		lang = DefaultDictionaryLanguage
	}
	return &Dictionary{
		db:   db,
		lang: lang,
	}, nil
}

func (d *Dictionary) close() error {
	return d.db.Close()
}

func (d *Dictionary) Search(ipAddress net.IP) (*GeoData, error) {

	// --- geoip2: the Domain method does not support the GeoLite2-City database
	// domain, err := d.db.Domain(ipAddress)
	// if err != nil {
	// 	return nil, err
	// }
	// var domain_name string
	// domain_name = domain.Domain

	record, err := d.db.City(ipAddress)
	if err != nil {
		return nil, err
	}
	//
	var country_code string
	country_code = record.Country.IsoCode

	var timezone string
	timezone = record.Location.TimeZone

	var ok bool
	var country_name string
	_, ok = record.Country.Names[d.lang]
	if ok {
		country_name = record.Country.Names[d.lang]
	}

	var city_name string
	_, ok = record.City.Names[d.lang]
	if ok {
		city_name = record.City.Names[d.lang]
	}

	return &GeoData{
		IPAddr: ipAddress.String(),
		// DomainName:  domain_name,
		CountryCode: country_code,
		CountryName: country_name,
		City:        city_name,
		TimeZone:    timezone,
	}, nil
}
