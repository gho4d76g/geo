package main

import (
	"bufio"
	"errors"
	"flag"
	"fmt"
	"net"
	"os"
	"strings"
)

type getGeo struct {
	logger *AppLogger
}

// Synopsis satisfied michellh/chi
func (c *getGeo) Synopsis() string {
	return "pearse Geo-data of IP address passed from standard input"
}

// Help satisfied micchell/chi
func (c *getGeo) Help() string {
	return "echo <IP-Address> | geo pearse (-output json) (-database PATH_TO_GEO_DATABASE_FILE)"
}

// Run satisfied micchell/chi
func (c *getGeo) Run(args []string) int {

	var ofs string
	var sPath string
	var cd string // country code
	flags := flag.NewFlagSet("getGeo", flag.ContinueOnError)
	// --output (-o) opton
	flags.StringVar(&ofs, "o", "", "output format")
	flags.StringVar(&ofs, "output", "", "output format")
	// --databasefile (-d) opton
	flags.StringVar(&sPath, "d", "GeoLite2-City.mmdb", "PATH to GeoIP2/GeoLite2 database file")
	flags.StringVar(&sPath, "database", "GeoLite2-City.mmdb", "PATH to GeoIP2/GeoLite2 database file")
	//	// --filter (-f) opton
	//	flags.StringVar(&cd, "f", "", "set CountryCode, you want to filtered")
	//	flags.StringVar(&cd, "filter", "", "set CountryCode, you want to filtered")

	if err := flags.Parse(args); err != nil {
		c.logger.Fatal(err)
		return ExitCodeError
	}
	c.logger.Debug("option -output: %s\n", ofs)
	c.logger.Debug("option -database: %s\n", sPath)
	//	c.logger.Debug("option -filter: %s\n", cd)

	// Load Data fron os.Stdin
	ipAddresses, err := loadAddress(os.Stdin)
	if err != nil {
		c.logger.Fatal(err)
		return ExitCodeError
	}
	c.logger.Debug("ipAddresses: %v\n", ipAddresses)

	// Open Geo Data Dictionaly
	dictionary, err := NewDictionary(sPath, DefaultDictionaryLanguage)
	if err != nil {
		c.logger.Fatal(err)
		return ExitCodeError
	}
	defer dictionary.close()

	var dataset GeoDataSet
	for _, ipAddress := range ipAddresses {
		c.logger.Debug("ipAddress: %v\n", ipAddress)
		data, err := dictionary.Search(ipAddress)
		if err != nil {
			c.logger.Fatal(err)
			return ExitCodeError
		}
		c.logger.Debug("data: %v\n", data)
		dataset.GeoIPs = append(dataset.GeoIPs, data)
	}

	if ofs == "json" {
		dataset.FprintJson(os.Stdout)
	} else if ofs == "tsv" {
		dataset.FprintTsv(os.Stdout)
	} else {
		dataset.Fprint(os.Stdout)
	}

	return ExitCodeOK
}

// loadAddress
func loadAddress(file *os.File) ([]net.IP, error) {
	var ipAddresses []net.IP

	s := bufio.NewScanner(file)
	for s.Scan() {
		ip := net.ParseIP(strings.TrimSpace(s.Text()))
		if ip != nil {
			ipAddresses = append(ipAddresses, ip)
		} else {
			// ToDo IPアドレスのバリデーション検討要
			return ipAddresses, errors.New(fmt.Sprintf("Invalid IPaddress input: %s", strings.TrimSpace(s.Text())))
		}
	}
	return ipAddresses, nil
}
