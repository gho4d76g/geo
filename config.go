package main

import (
	"io"
	"os"
)

type Config struct {
	outStream, errStream io.Writer
}

func NewConfig() *Config {
	return &Config{
		outStream: os.Stdout,
		errStream: os.Stderr,
	}
}
