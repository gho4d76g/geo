package main

import (
	"math/rand"
	"os"
	"time"

	"github.com/mitchellh/cli"
)

const (
	ExitCodeOK        int = iota // 0
	ExitCodeError                // 1
	ExitCodeFileError            // 2
)

func main() {
	rand.Seed(time.Now().UTC().UnixNano())
	app := NewApp()
	c := cli.NewCLI(app.name, app.version)
	c.Args = os.Args[1:]
	c.Commands = map[string]cli.CommandFactory{
		// sucommand: statistics
		"pearse": func() (cli.Command, error) {
			return &getGeo{
				logger: app.logger,
			}, nil
		},
	}
	exitStatus, err := c.Run()
	if err != nil {
		app.logger.Fatal(err)
	}
	os.Exit(exitStatus)
}
